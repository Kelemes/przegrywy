module SessionsHelper

  def log_in (user)
    session[:user_id] = user.id
  end

  def is_member?
    current_user.member?
  end

  def member_user
          unless is_member?
            store_location
            flash[:danger] = "Brak uprawnień"
            redirect_to root_url
          end
        end

  def logged_in_user
        unless logged_in?
          store_location
          flash[:danger] = "Zaloguj się"
          redirect_to log_in_url
        end
      end


  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
    if (user_id = session[:user_id])
          @current_user ||= User.find_by(id: user_id)
        elsif (user_id = cookies.signed[:user_id])
          user = User.find_by(id: user_id)
          if user && user.authenticated?(cookies[:remember_token])
            log_in user
            @current_user = user
          end
        end
  end

  def current_user?(user)
     user == current_user
   end


  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  def logged_in?
    !current_user.nil?
  end

  def logout
    session.delete(:user_id)
    forget(@current_user)
    @current_user = nil
  end


  def remember(user)
    user.remember
    cookies.permanent[:remember_token] = user.remember_token
    cookies.permanent.signed[:user_id] = user.id
  end

  # Redirects to stored location (or to the default).
    def redirect_back_or(default)
      redirect_to(session[:forwarding_url] || default)
      session.delete(:forwarding_url)
    end

    # Stores the URL trying to be accessed.
    def store_location
      session[:forwarding_url] = request.original_url if request.get?
    end
end
