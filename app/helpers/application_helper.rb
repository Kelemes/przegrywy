module ApplicationHelper
  def full_title (page_title = '')
    title = "Przegrywy"
    if page_title.empty?
      title
    else
      page_title + " | " + title
    end
  end

  def list_categories
    %w[biurowe elektronika narzędzia inne]
  end

  def list_states
    #usunąłem wypożyczony, bo tylko wypożyczenie może go ustalić
    %w[nowy sprawny uszkodzony zniszczony]
  end
end
