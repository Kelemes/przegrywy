class ProjectMembershipsController < ApplicationController

  before_action :coordinator_user

  def new
    @users = User.paginate(page: params[:page])
  end

  def create
    @project_membership = ProjectMembership.new(relation_params)
    if @project_membership.save
      flash[:success] = "Użytkownik dodany do projektu!"
    else
      flash[:danger] = "Błąd!"
    end
    redirect_to (new_project_member_path(@project_membership.project_id))
  end

  def destroy
    @project_membership = ProjectMembership.find_by(relation_params)
    @project_membership.historical = true
    if @project_membership.save
      flash[:success] = "Użytkownik usunięty z projektu!"
    else
      flash[:danger] = "Błąd!"
    end
    redirect_to (project_path(params[:project_id]))
  end

  private
  def relation_params
    params.permit(:member_id,:project_id)
  end

end
