class ProjectsController < ApplicationController
  before_action :logged_in_user
  before_action :coordinator_user, only: [:update, :new, :create, :destroy]

  def index
    @projects = Project.search(params[:term])
  end

  def show

    @project = Project.find_by(id: params[:id])
    @members = @project.members.paginate(page: params[:page])
    @relations = @project.project_memberships
    @raports = @project.raports
    @things = @project.things

    @section_membership = @project.project_section_relations.build(project_id: @project.id) if current_user.coordinator?
    @sections = []
    if current_user.coordinator?
      Section.find_each do |section|
        @sections << section if !section.projects.include?(@project)
      end
    end
  end

  def create
      @project = Project.new(project_params)
      if @project.save
        flash[:success] = "Projekt dodany pomyślnie!"
        redirect_to projects_path
      else
        flash[:danger] = "Prawdpodobnie ta nazwa jest już zajęta"
        render 'new'
      end
    end

  def update
    project = Project.find_by(id: params[:project_id])
    user = User.find_by(id: params[:member_id])
    project.coordinator = user
    if project.save
      flash[:success] = "Użytkownik mianonowany koordynatorem"
    else
      flash[:danger] = "błąd!"
    end
    redirect_to project_path(params[:project_id])
  end

  def new
    @project = Project.new
  end

  def destroy
    @project = Project.find_by(id: params[:id])
    @project.finished_at = Time.now.utc
    if @project.save
      flash[:success] = "Projekt zarchiwzowany!"
    else
      flash[:danger] = "Błąd!"
    end
    redirect_to projects_path
  end

private

  def project_params
    params.require(:project).permit(:name)
  end
end
