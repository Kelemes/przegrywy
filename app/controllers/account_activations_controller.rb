class AccountActivationsController < ApplicationController

  before_action :coordinator_user


  def activate
    user= User.find_by(id: params[:id])
    user.update_attribute(:activated,    true)
    user.update_attribute(:activated_at, Time.zone.now)
    flash[:success] = "Konto pomyślnie aktywowane"
    redirect_to users_path
  end

  def promote_to_member
    user= User.find_by(id: params[:id])
    if user.activated?
      user.update_attribute(:member,    true)
     flash[:success] = "Użytkownik awansowany na członka"
      redirect_to user_path(user)
    else
      redirect_to root_url
      flash[:error] = "Najpierw aktywuj użytkownika"
    end
  end

private
def coordinator_user
  redirect_to(root_url) unless current_user.coordinator?
end




end
