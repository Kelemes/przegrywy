class MaterialsController < ApplicationController
  before_action :logged_in_user, only: [:index, :show, :edit, :update]
  before_action :member_user, only: [:new, :create]

  def new
    @material = Material.new(supervisor_id: current_user.id)
    @categories = list_categories
  end

  def create
    @material = Material.new(material_params)
    if @material.save
      flash[:success] = "Materiał dodany pomyślnie!"
      redirect_to materials_path
    else
      flash[:danger] = "Niepoprawny przedmiot"
      render 'new'
    end
  end

  def update
    @material = Material.find(params[:id])
    if @material.update_attributes(material_params)
       flash[:success] = "Materiał eksploatacyjny pomyślnie zaktualizowany"
      redirect_to @material
    else
      render 'edit'
    end
  end

  def show
    @material = Material.find(params[:id])
    @user = @material.supervisor
  end

  def index
    @materials = Material.search(params[:term]).paginate(page: params[:page])
  end

  def edit
    @material = Material.find(params[:id])
    @categories = list_categories
  end


  private
  def material_params
    params.require(:material).permit(:name, :supervisor_id, :note, :category, :amount)
  end


end
