class RaportsController < ApplicationController
  before_action :logged_in_user
  before_action :user_belongs_to_project?, only: [:new, :create]

  def new
    @raport = Raport.new(project_id: params[:project_id], author_id: current_user.id)
  end

  def create
    raport = Raport.new(raport_params)
    if raport.save
      flash[:success] = "Raport dodany"
    else
      flash[:danger] = "Błąd"
    end
    redirect_to project_path(raport.project_id)
  end

  def show
    @raport = Raport.find(params[:id])
    @author = User.find_by(id: @raport.author_id)
    @project = Project.find_by(id: @raport.project_id)
  end

private

    def user_belongs_to_project?
      @project = Project.find_by(id: params[:project_id]) ||  Project.find_by(id: params[:raport][:project_id])
     redirect_to(root_url) unless current_user.projects.include?(@project)
    end

    def raport_params
        params.require(:raport).permit(:project_id, :author_id, :content)
      end
end
