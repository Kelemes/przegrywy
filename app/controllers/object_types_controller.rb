class ObjectTypesController < ApplicationController

  before_action :logged_in_user, only: [:index, :show]
  before_action :member_user, only: [:new, :create, :update, :edit, :destroy]

  def index
    @object_types = ObjectType.paginate(page: params[:page])
  end

  def show
    @object_type = ObjectType.find(params[:id])
  end

  def edit
    @object_type = ObjectType.find(params[:id])
    @categories = list_categories
  end

  def update
    @object_type = ObjectType.find(params[:id])
    if @object_type.update_attributes(object_type_params)
       flash[:success] = "Typ pomyślnie zaktualizowany"
      redirect_to @object_type
    else
      render 'edit'
    end
  end

  def destroy
    ObjectType.find(params[:id]).destroy
    flash[:success] = "Typ usunięty"
    redirect_to object_types_path
  end

  def new
    @object_type = ObjectType.new
    @categories = list_categories
  end

  def create
    @object_type = ObjectType.new(object_type_params)
    if @object_type.save
      flash[:success] = "Typ dodany pomyślnie!"
      redirect_to object_types_path
    else
      flash[:danger] = "Taki typ już istnieje lub niepodano nazwy"
      redirect_to new_object_type_path
    end
  end

  private
  def object_type_params
    params.require(:object_type).permit(:name, :note, :category)
  end
end
