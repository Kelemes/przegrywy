class ThingsController < ApplicationController
    before_action :logged_in_user, only: [:index, :show]
    before_action :member_user, only: [:new, :create, :edit, :update]

    def new
      @thing = Thing.new(supervisor_id: current_user.id, state: "nowy")
      @object_types = ObjectType.all
    end

    def create
      @thing = Thing.new(thing_params)
      if @thing.save
        flash[:success] = "Egzemplarz dodany pomyślnie!"
        redirect_to things_path
      else
        flash[:danger] = "Numer seryjny zajęty lub pusty"
        redirect_to new_thing_path
      end
    end

    def update
      @thing = Thing.find(params[:id])
      if @thing.update_attributes(thing_params)
         flash[:success] = "Egzemplarz pomyślnie zaktualizowany"
        redirect_to @thing
      else
        redirect_to edit_thing_path(@thing)
      end
    end

    def show
      @thing = Thing.find(params[:id])
      @supervisor = @thing.supervisor
      @borrower = @thing.borrower
    end

    def index
      @things = Thing.search(params[:term]).paginate(page: params[:page])
    end

    def edit
      @thing = Thing.find(params[:id])
      @states = list_states
      @object_types = ObjectType.all
    end


  def define_supervisor
  end




  private
  def thing_params
    params.require(:thing).permit(:serial_number, :supervisor_id, :note, :object_type_id, :state)
  end
end
