class SectionMembershipsController < ApplicationController
  before_action :coordinator_user

  def create
    @relation = SectionMembership.new(relation_params)
    if @relation.save
      flash[:success] = "Użytkownik dodany do sekcji!"
      redirect_to (user_path(@relation.member_id))
    else
      flash[:danger] = "Błąd!"
      redirect_to (user_path(@relation.member_id))
    end
  end

  def destroy
    @relation = SectionMembership.find_by(destroing_params)
    @relation.historical = true
    if @relation.save
      flash[:success] = "Użytkownik usunięty z sekcji!"
      redirect_to (section_path(@relation.section))
    else
      flash[:danger] = "Błąd!"
      redirect_to (section_path(@relation.section))
    end
  end

  private
  def relation_params
    params.require(:section_membership).permit(:member_id,:section_id)
  end

  def destroing_params
    params.permit(:member_id,:section_id)
  end
end
