class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include ApplicationHelper
  include SessionsHelper

  def coordinator_user
    redirect_to(root_url) unless current_user.coordinator?
  end

end
