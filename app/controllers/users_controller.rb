class UsersController < ApplicationController


  before_action :logged_in_user, only: [:index, :show, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]
  before_action :coordinator_user,     only: [:destroy]


  def new
    @user = User.new
  end

  def show
      @user=User.find(params[:id])
      @section_membership = @user.section_memberships.build(member_id: @user.id) if current_user.coordinator?
      @sections = []
      if current_user.coordinator?
        Section.find_each do |section|
          @sections << section if !section.members.include?(@user)
        end
      end
  end

  def index
    @users = User.search(params[:term]).paginate(page: params[:page])
  end

  def destroy
      User.find(params[:id]).destroy
      flash[:success] = "Użytkownik usunięty"
      redirect_to users_url
    end


  def create
      @user = User.new(user_params)
      if @user.save
        flash[:success] = "Konto utworzone pomyślnie!"
        redirect_to root_path
      else
        render 'new'
      end
    end

    def edit
       @user = User.find(params[:id])
     end


     def update
       @user = User.find(params[:id])
       if @user.update_attributes(user_params)
          flash[:success] = "Profil pomyślnie zaktualizowany"
         redirect_to @user
       else
         render 'edit'
       end
     end

    private

      def user_params
        params.require(:user).permit(:name, :surname, :email, :password,
                                     :password_confirmation, :phone_number)
      end



      def correct_user
       @user = User.find(params[:id])
       redirect_to(root_url) unless current_user?(@user) || current_user.coordinator?
      end



end
