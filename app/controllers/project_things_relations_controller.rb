class ProjectThingsRelationsController < ApplicationController
  before_action :member_user

  def new
    @projects = Project.paginate(page: params[:page])
  end

  def create
    @project_things_relation = ProjectThingsRelation.new(relation_params)
    if @project_things_relation.save
      flash[:success] = "Przedmiot dodany do projektu!"
    else
      flash[:danger] = "Błąd!"
    end
    redirect_to(add_thing_to_project_path(@project_things_relation.thing))
  end

  def destroy
    relation = ProjectThingsRelation.find(params[:id])
    project = relation.project
    relation.destroy
      flash[:success] = "Przedmiot usunięty z projektu!"
    redirect_to project_path(project)
  end

  private
  def relation_params
    params.permit(:thing_id,:project_id)
  end
end
