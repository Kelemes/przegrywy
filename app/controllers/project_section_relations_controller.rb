class ProjectSectionRelationsController < ApplicationController
  before_action :coordinator_user
  before_action :project_is_alive

  def create
    @relation = ProjectSectionRelation.new(relation_params)
    if @relation.save
      flash[:success] = "Projekt dodany do sekcji!"
    else
      flash[:danger] = "Błąd!"
    end
    redirect_to (project_path(@relation.project_id))
  end

  private
  def relation_params
    params.require(:project_section_relation).permit(:project_id,:section_id)
  end

  def project_is_alive
    redirect_to root_path unless Project.find_by(id: params[:project_section_relation][:project_id]).finished_at.nil?
  end

end
