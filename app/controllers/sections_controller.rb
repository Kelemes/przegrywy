class SectionsController < ApplicationController

  before_action :logged_in_user
  before_action :coordinator_user, only: [:update]

  def index
    @sections = Section.all
  end

  def show

    @section = Section.find_by(id: params[:id])
    @members = @section.members.paginate(page: params[:page])
    @relations = @section.section_memberships
  end

  def update
    section = Section.find_by(id: params[:section_id])
    user = User.find_by(id: params[:member_id])
    section.coordinator = user
    if section.save
      redirect_to section_path(params[:section_id])
    else
      flash[:danger] = "błąd!"
      redirect_to section_path(params[:section_id])
    end
  end
end
