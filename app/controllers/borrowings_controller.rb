class BorrowingsController < ApplicationController
  before_action :member_user

  def create
    @borrowing = Borrowing.new(borrowing_params)
    @thing = Thing.find(params[:borrow][:thing_id])
    if @thing.borrowings.where(returned_at: nil).empty? && @borrowing.save
      flash[:success] = "Przedmiot został wypożyczony!"
    else
      flash[:danger] = "Nie udało się wypożyczyć"
    end
    redirect_to things_path
  end

  def finish
    @borrowing = Borrowing.where(thing_id: params[:thing_id],
                        user_id: current_user.id).last
    @borrowing.finish
    flash[:success] = "Przedmiot zwrócony"
    redirect_to user_path(current_user)
  end

private
  def borrowing_params
    params.require(:borrow).permit(:thing_id, :user_id)
  end
end
