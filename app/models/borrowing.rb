class Borrowing < ApplicationRecord
  belongs_to :user
  belongs_to :thing
  validates :user_id, presence: true
  validates :thing_id, presence: true


  def finish
    update_attribute(:returned_at, Time.now)
  end
end
