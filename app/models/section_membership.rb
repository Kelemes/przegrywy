class SectionMembership < ApplicationRecord
  belongs_to :section
  belongs_to :member, class_name: "User"
  validates :member_id, presence: true
  validates :section_id, presence: true
#  default_scope  {where (historical: false)}
end
