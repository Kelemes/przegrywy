class ProjectThingsRelation < ApplicationRecord
  belongs_to :project
  belongs_to :thing
  validates :project_id, presence: true
  validates :thing_id, presence: true
end
