class ObjectType < ApplicationRecord
  validates :name, presence: true, uniqueness: true,
            length: { maximum: 50 }
  validates :category, presence: true
  has_many :things
end
