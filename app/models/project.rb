class Project < ApplicationRecord
  validates :name, presence: true, length: { maximum: 50 }, uniqueness: true

  belongs_to :coordinator, class_name: "User", optional: true

  has_many :members, through: :project_memberships, source: :member
  has_many :project_memberships, foreign_key: "project_id"

  has_many :sections, through: :project_section_relations
  has_many :project_section_relations, foreign_key: "project_id"

  has_many :raports, foreign_key: "project_id"

  has_many :project_things_relations
  has_many :things, through: :project_things_relations

  def self.search(pattern)
    if pattern.blank?  # blank? covers both nil and empty string
      all
    else
      left_outer_joins(:coordinator).where(
        "projects.name LIKE ? OR
         users.name || ' ' || users.surname
         LIKE ? OR
        CAST(projects.id AS TEXT) LIKE ?",
        "%#{pattern}%","%#{pattern}%",
        "#{pattern}")
    end
  end


end
