class ProjectSectionRelation < ApplicationRecord
  belongs_to :project
  belongs_to :section
  validates :section_id, presence: true
  validates :project_id, presence: true
end
