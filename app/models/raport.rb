class Raport < ApplicationRecord
  validates :content, presence: true, length: { maximum: 5000 }
  belongs_to :author, class_name: "User"
  belongs_to :project
  validates :author_id, presence: true
  validates :project_id, presence: true
end
