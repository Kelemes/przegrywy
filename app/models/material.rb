class Material < ApplicationRecord
  default_scope -> { order(:updated_at)}
  validates :name, presence: true, length: { maximum: 50 }, uniqueness: true
  validates :amount, presence: true
  validates :category, presence: true
  belongs_to :supervisor, class_name: "User", optional: true



  def self.search(pattern)
    if pattern.blank?  # blank? covers both nil and empty string
      all
    else
      left_outer_joins(:supervisor).where(
        "materials.name LIKE ? OR
         materials.category LIKE ? OR
         CAST(materials.amount AS TEXT) LIKE ? OR
         materials.note LIKE ? OR
         users.name || ' ' || users.surname LIKE ? OR
        CAST(materials.id AS TEXT) LIKE ?",
        "%#{pattern}%","%#{pattern}%",
        "#{pattern}","%#{pattern}%","%#{pattern}%",
        "#{pattern}")
    end
  end

end
