class ProjectMembership < ApplicationRecord
  belongs_to :project
  belongs_to :member, class_name: "User"
  validates :member_id, presence: true
  validates :project_id, presence: true
end
