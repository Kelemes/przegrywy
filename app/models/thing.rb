class Thing < ApplicationRecord
  validates :serial_number, presence: true, uniqueness: true,
            length: { maximum: 50 }
  validates :state, presence: true
  belongs_to :object_type
  belongs_to :supervisor, class_name: "User"
  has_many :project_things_relations
  has_many :projects, through: :project_things_relations
  has_many :borrowings
  has_many :borrowers, through: :borrowings, source: :user


  def borrower
    self.borrowers.last
  end

  def self.search(pattern)
    if pattern.blank?  # blank? covers both nil and empty string
      all
    elsif pattern == "#wypożyczony"
      joins(:borrowings).where(borrowings: {returned_at: nil})
    else
      left_outer_joins(:supervisor,:object_type).where(
        "CAST(things.id AS TEXT) LIKE ? OR
         things.note LIKE ? OR
         object_types.name LIKE ? OR
         things.state LIKE ? OR
         things.serial_number LIKE ? OR
         users.name || ' ' || users.surname LIKE ?",
        "#{pattern}","%#{pattern}%","%#{pattern}%",
        "%#{pattern}%","%#{pattern}%",
        "#{pattern}")
    end
  end

end
