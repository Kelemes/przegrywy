class User < ApplicationRecord
  attr_accessor :remember_token

  before_save { self.email.downcase! }

  default_scope -> { order(:surname, :name)}
  validates :name, presence: true, length: { maximum: 50 }
  validates :surname, presence: true, length: { maximum: 50 }
    VALID_EMAIL_REGEX = /[\w+\-.]+@[a-z\d\-.]*[a-z\d]\.[a-z]+/i
  validates :email, presence: true,
            length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: {case_sensitive: false}

  has_secure_password
  validates :password, presence: true, length: { minimum: 8 }, allow_nil: true



  has_many :section_memberships,   foreign_key: "member_id"
  has_many :sections, through: :section_memberships
  has_many :coordinates, class_name: "Section", foreign_key: "coordinator_id"

  has_many :project_memberships,   foreign_key: "member_id"
  has_many :projects, through: :project_memberships
  has_many :coordinates_projects, class_name: "Project", foreign_key: "coordinator_id"

  has_many :raports, foreign_key: "author_id"

  has_many :materials, foreign_key: "supervisor_id"

  has_many :things, foreign_key: "supervisor_id"

  has_many :borrowings
  has_many :borrowed, through: :borrowings, source: :thing


  def self.new_token
     SecureRandom.urlsafe_base64
   end

   def remember
       self.remember_token = User.new_token
       update_attribute(:remember_digest, User.digest(remember_token))
     end

     def authenticated?(remember_token)
      return false if self.remember_digest.nil?
         BCrypt::Password.new(remember_digest).is_password?(remember_token)
       end

  def forget
      update_attribute(:remember_digest, nil)
    end

  def self.digest(string)
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                    BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
  end

  def currently_borrowed
    borrowings = self.borrowings.where(returned_at: nil)
    borrowed_things = []
    borrowings.each do |borrowing|
      borrowed_things << borrowing.thing
    end
    borrowed_things
  end

  def self.search(pattern)
    if pattern.blank?  # blank? covers both nil and empty string
      all
    else
      if pattern == "#aktywny"
        where(activated: true)
      elsif pattern == "#koordynator"
        where(coordinator: true)
      elsif pattern == "#członek"
        where(member: true)
      elsif pattern == "#opiekun"
        where(supervisor: true)
      else
        where(
          "CAST(users.id AS TEXT) LIKE ? OR
           users.name LIKE ? OR
           users.surname LIKE ? OR
           users.name || ' ' || users.surname LIKE ? OR
           users.phone_number LIKE ?",
          "#{pattern}","%#{pattern}%","%#{pattern}%",
          "%#{pattern}%","%#{pattern}%")
      end
    end
  end

end
