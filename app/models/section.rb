class Section < ApplicationRecord
  validates :name, presence: true, length: { maximum: 50 }, uniqueness: true
  belongs_to :coordinator, class_name: "User"
  has_many :members, through: :section_memberships, source: :member
  has_many :section_memberships, foreign_key: "section_id"
  has_many :projects, through: :project_section_relations
  has_many :project_section_relations, foreign_key: "section_id"

end
