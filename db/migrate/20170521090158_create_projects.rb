class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.datetime :finished_at, null: true
      t.integer :coordinator_id
      t.string :name

      t.timestamps
    end
    add_index :projects, :name, unique: true
    add_index :projects, :finished_at
  end
end
