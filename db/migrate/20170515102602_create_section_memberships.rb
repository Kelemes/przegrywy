class CreateSectionMemberships < ActiveRecord::Migration[5.0]
  def change
    create_table :section_memberships do |t|
      t.integer :section_id
      t.integer :member_id

      t.timestamps
    end
    add_index :section_memberships, :section_id
    add_index :section_memberships, :member_id
    add_index :section_memberships, [:section_id, :member_id], unique: true

  end
end
