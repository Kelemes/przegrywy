class CreateObjectTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :object_types do |t|
      t.text :note
      t.string :name
      t.string :category

      t.timestamps
    end
    add_index :object_types, :name, unique: true
    add_index :object_types, :category
  end
end
