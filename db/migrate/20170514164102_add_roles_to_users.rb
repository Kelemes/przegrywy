class AddRolesToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :coordinator, :boolean, default: false
    add_column :users, :activated, :boolean, default: false
    add_column :users, :member, :boolean, default: false
    add_column :users, :supervisor, :boolean, default: false
  end
end
