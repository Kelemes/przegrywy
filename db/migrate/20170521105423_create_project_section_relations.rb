class CreateProjectSectionRelations < ActiveRecord::Migration[5.0]
  def change
    create_table :project_section_relations do |t|
      t.integer :section_id
      t.integer :project_id

      t.timestamps
    end
    add_index :project_section_relations, :project_id
        add_index :project_section_relations, :section_id
        add_index :project_section_relations, [:project_id, :section_id], unique: true
  end
end
