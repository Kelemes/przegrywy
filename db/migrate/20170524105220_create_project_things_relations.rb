class CreateProjectThingsRelations < ActiveRecord::Migration[5.0]
  def change
    create_table :project_things_relations do |t|
      t.integer :project_id
      t.integer :thing_id

      t.timestamps
    end
    add_index :project_things_relations, :project_id
        add_index :project_things_relations, :thing_id
        add_index :project_things_relations, [:project_id, :thing_id], unique: true
  end
end
