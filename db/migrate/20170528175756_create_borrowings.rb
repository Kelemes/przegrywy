class CreateBorrowings < ActiveRecord::Migration[5.0]
  def change
    create_table :borrowings do |t|
      t.datetime :returned_at, null: true
      t.integer :thing_id
      t.integer :user_id

      t.timestamps
    end
    add_index :borrowings, :user_id
    add_index :borrowings, :thing_id
    add_index :borrowings, :returned_at

  end
end
