class CreateMaterials < ActiveRecord::Migration[5.0]
  def change
    create_table :materials do |t|
      t.integer :supervisor_id
      t.integer :amount
      t.string :name
      t.string :category
      t.text :note

      t.timestamps
    end
    add_index :materials, :supervisor_id
    add_index :materials, :category
    add_index :materials, :name, unique: true
  end
end
