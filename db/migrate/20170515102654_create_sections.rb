class CreateSections < ActiveRecord::Migration[5.0]
  def change
    create_table :sections do |t|
      t.string :name
      t.integer :coordinator_id

      t.timestamps
    end
    add_index :sections, :name, unique: true
  end
end
