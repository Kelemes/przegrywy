class CreateProjectMemberships < ActiveRecord::Migration[5.0]
  def change
    create_table :project_memberships do |t|
      t.integer :member_id
      t.integer :project_id
      t.boolean :historical, default: false

      t.timestamps
    end
    add_index :project_memberships, :project_id
        add_index :project_memberships, :member_id
        add_index :project_memberships, :historical
        add_index :project_memberships, [:project_id, :member_id], unique: true
  end
end
