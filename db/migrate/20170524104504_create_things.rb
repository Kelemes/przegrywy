class CreateThings < ActiveRecord::Migration[5.0]
  def change
    create_table :things do |t|
      t.text :note
      t.integer :object_type_id
      t.string :state
      t.string :serial_number
      t.integer :supervisor_id

      t.timestamps
    end
    add_index :things, :serial_number, unique: true
    add_index :things, :state
    add_index :things, :object_type_id
    add_index :things, :supervisor_id
  end
end
