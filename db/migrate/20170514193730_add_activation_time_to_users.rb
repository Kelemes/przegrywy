class AddActivationTimeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :activated_at, :datetime
  end
  add_index :users, :activated_at
  add_index :users, :member
  add_index :users, :coordinator
  add_index :users, :supervisor
  add_index :users, :activated
end
