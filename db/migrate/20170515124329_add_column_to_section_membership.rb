class AddColumnToSectionMembership < ActiveRecord::Migration[5.0]
  def change
    add_column :section_memberships, :historical, :boolean, default: false
    add_index :section_memberships, :historical
  end
end
