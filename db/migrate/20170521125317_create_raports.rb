class CreateRaports < ActiveRecord::Migration[5.0]
  def change
    create_table :raports do |t|
      t.text :content
      t.integer :author_id
      t.integer :project_id

      t.timestamps
    end
    add_index :raports, :author_id
    add_index :raports, :project_id
  end
end
