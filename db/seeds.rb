User.create!(name:  "Artur",
             surname: "AAAcki",
             email: "psyduck3@tlen.pl",
             password:              "12345678",
             password_confirmation: "12345678",
             activated: true,
             coordinator: true)



User.create!(name:  "Example",
             surname: "User",
             email: "example@example.com",
             password:              "12345678",
             password_confirmation: "12345678")


99.times do |n|
  name  = Faker::Name.first_name
  surname  = Faker::Name.last_name
  email = "example-#{n+1}@example.com"
  password = "12345678"
  User.create!(name:  name,
               surname: surname,
               email: email,
               phone_number: n*n,
               password:              password,
               password_confirmation: password)
end

Section.create!(name: "Rakietowa", coordinator_id: 1)
Section.create!(name: "Robotyczna", coordinator_id: 2)
Section.create!(name: "Balonowa", coordinator_id: 1)
Section.create!(name: "PW-Sat", coordinator_id: 1)


SectionMembership.create!(member_id: 1,
                          section_id: 2)

SectionMembership.create!(member_id: 1,
                          section_id: 3)

SectionMembership.create!(member_id: 2,
                          section_id: 2)


50.times do |n|
  name = Faker::Name.last_name
  Project.create(name: name,
                  coordinator_id: 1)
end
