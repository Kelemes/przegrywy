# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170528175756) do

  create_table "borrowings", force: :cascade do |t|
    t.datetime "returned_at"
    t.integer  "thing_id"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["returned_at"], name: "index_borrowings_on_returned_at"
    t.index ["thing_id"], name: "index_borrowings_on_thing_id"
    t.index ["user_id"], name: "index_borrowings_on_user_id"
  end

  create_table "materials", force: :cascade do |t|
    t.integer  "supervisor_id"
    t.integer  "amount"
    t.string   "name"
    t.string   "category"
    t.text     "note"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["category"], name: "index_materials_on_category"
    t.index ["name"], name: "index_materials_on_name", unique: true
    t.index ["supervisor_id"], name: "index_materials_on_supervisor_id"
  end

  create_table "object_types", force: :cascade do |t|
    t.text     "note"
    t.string   "name"
    t.string   "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category"], name: "index_object_types_on_category"
    t.index ["name"], name: "index_object_types_on_name", unique: true
  end

  create_table "project_memberships", force: :cascade do |t|
    t.integer  "member_id"
    t.integer  "project_id"
    t.boolean  "historical", default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["historical"], name: "index_project_memberships_on_historical"
    t.index ["member_id"], name: "index_project_memberships_on_member_id"
    t.index ["project_id", "member_id"], name: "index_project_memberships_on_project_id_and_member_id", unique: true
    t.index ["project_id"], name: "index_project_memberships_on_project_id"
  end

  create_table "project_section_relations", force: :cascade do |t|
    t.integer  "section_id"
    t.integer  "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id", "section_id"], name: "index_project_section_relations_on_project_id_and_section_id", unique: true
    t.index ["project_id"], name: "index_project_section_relations_on_project_id"
    t.index ["section_id"], name: "index_project_section_relations_on_section_id"
  end

  create_table "project_things_relations", force: :cascade do |t|
    t.integer  "project_id"
    t.integer  "thing_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id", "thing_id"], name: "index_project_things_relations_on_project_id_and_thing_id", unique: true
    t.index ["project_id"], name: "index_project_things_relations_on_project_id"
    t.index ["thing_id"], name: "index_project_things_relations_on_thing_id"
  end

  create_table "projects", force: :cascade do |t|
    t.datetime "finished_at"
    t.integer  "coordinator_id"
    t.string   "name"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["finished_at"], name: "index_projects_on_finished_at"
    t.index ["name"], name: "index_projects_on_name", unique: true
  end

  create_table "raports", force: :cascade do |t|
    t.text     "content"
    t.integer  "author_id"
    t.integer  "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_raports_on_author_id"
    t.index ["project_id"], name: "index_raports_on_project_id"
  end

  create_table "section_memberships", force: :cascade do |t|
    t.integer  "section_id"
    t.integer  "member_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "historical", default: false
    t.index ["historical"], name: "index_section_memberships_on_historical"
    t.index ["member_id"], name: "index_section_memberships_on_member_id"
    t.index ["section_id", "member_id"], name: "index_section_memberships_on_section_id_and_member_id", unique: true
    t.index ["section_id"], name: "index_section_memberships_on_section_id"
  end

  create_table "sections", force: :cascade do |t|
    t.string   "name"
    t.integer  "coordinator_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "things", force: :cascade do |t|
    t.text     "note"
    t.integer  "object_type_id"
    t.string   "state"
    t.string   "serial_number"
    t.integer  "supervisor_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["object_type_id"], name: "index_things_on_object_type_id"
    t.index ["serial_number"], name: "index_things_on_serial_number", unique: true
    t.index ["state"], name: "index_things_on_state"
    t.index ["supervisor_id"], name: "index_things_on_supervisor_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "surname"
    t.string   "phone_number"
    t.string   "section_name"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "coordinator",     default: false
    t.boolean  "activated",       default: false
    t.boolean  "member",          default: false
    t.boolean  "supervisor",      default: false
    t.datetime "activated_at"
    t.index ["activated"], name: "index_users_on_activated"
    t.index ["activated_at"], name: "index_users_on_activated_at"
    t.index ["coordinator"], name: "index_users_on_coordinator"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["member"], name: "index_users_on_member"
    t.index ["supervisor"], name: "index_users_on_supervisor"
  end

end
