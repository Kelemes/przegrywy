require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  test "should get root" do
      get root_path
      assert_response :success
      assert_select "title", "Strona startowa | Przegrywy"
    end

  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "O nas | Przegrywy"

  end

  test "should get rules" do
    get rules_path
    assert_response :success
    assert_select "title", "Regulamin | Przegrywy"
  end

end
