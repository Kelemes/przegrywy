require 'test_helper'

class SectionMembershipsControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get section_memberships_create_url
    assert_response :success
  end

  test "should get destroy" do
    get section_memberships_destroy_url
    assert_response :success
  end

end
