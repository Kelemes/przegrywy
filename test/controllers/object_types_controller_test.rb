require 'test_helper'

class ObjectTypesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get object_types_index_url
    assert_response :success
  end

  test "should get show" do
    get object_types_show_url
    assert_response :success
  end

  test "should get new" do
    get object_types_new_url
    assert_response :success
  end

end
