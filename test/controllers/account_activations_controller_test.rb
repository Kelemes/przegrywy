require 'test_helper'

class AccountActivationsControllerTest < ActionDispatch::IntegrationTest
  test "should get activate" do
    get account_activations_activate_url
    assert_response :success
  end

end
