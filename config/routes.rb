Rails.application.routes.draw do
  post 'things/borrow', to: 'borrowings#create', as: 'borrow'
  post 'things/return', to: 'borrowings#finish', as: 'return'

  get 'things/:id/add_project', to: 'project_things_relations#new', as: 'add_thing_to_project'

  get '/material/:id', to: 'materials#change_amount', as: 'change_amount'

  get 'raports/new'

  post '/project_section_relations/create', to: 'project_section_relations#create', as: 'add_project_to_section'

  post 'project/coordinate', to: 'projects#update', as: 'coordinate_project'
  post 'project/destroy_member', to: 'project_memberships#destroy', as: 'destroy_project_member'
  post 'project/add_member', to: 'project_memberships#create', as: 'create_project_member'
  get 'project/:id/add_member', to: 'project_memberships#new', as: 'new_project_member'

  post '/new_project', to: 'projects#create'
  get '/new_project', to: 'projects#new'

  post 'sections/index/coordinate', to: 'sections#update', as: 'coordinate'

  get 'sections/index'

  post 'section_memberships/create', to: 'section_memberships#create', as: 'add_membership'

  post 'section_memberships/destroy', to: 'section_memberships#destroy', as: 'destroy_membership'

  get '/activate/:id', to: 'account_activations#activate', as: 'activate'
  get '/promote/:id', to: 'account_activations#promote_to_member', as: 'promote'

  get '/log_in', to: 'sessions#new'
  post '/log_in', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'

  get '/about',  to: 'static_pages#about'

  get '/rules', to: 'static_pages#rules'

root 'static_pages#home'

resources :users
resources :section_memberships, only: [:destroy, :create]
resources :sections, only: [:index, :show]
resources :projects, only: [:index, :show, :new, :update, :destroy]
resources :raports, only: [:new, :show, :create]
resources :materials
resources :object_types
resources :things
resources :project_things_relations, only: [:create, :destroy]


end
